//
//  Created by Christopher Saez on 24/04/15.
//  Copyright (c) 2015 kairos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kAllowKNotification @"com_kairos_beaconManager_allowNotification"

@protocol KairosBeaconManagerDelegate <NSObject>

-(void) onError:(NSString * __nullable) message;

@end

@interface KairosBeaconManager : NSObject<CLLocationManagerDelegate>


/**
 *  When the beacon manager enters a region, 
 * it actively scans for beacons during the scanDelay.
 * The default scan delay is 8 seconds and can be tweaked by setting directly this value.
 */
@property (assign, nonatomic) NSInteger scanDelay;

/**
 *  Sets the beacon manager delegate.
 */
@property (strong, nonatomic, nullable) id<KairosBeaconManagerDelegate> delegate;

/**
 * Get KairosBeaconManager singleton
 *
 * @return KairosbeaconManager singleton
**/
+ (KairosBeaconManager * __nullable)sharedInstance:(NSString * __nonnull) secret
                                          appToken:(NSString * __nonnull) appToken
                                          delegate:(id<KairosBeaconManagerDelegate> __nonnull) delegate;


/**
 *  When set to YES, shows KairosBeaconManager logs
 *
 *  @param debug <#debug description#>
 */
-(void) setDebug:(BOOL) debug;

/**
 *  Start beacon region monitoring
 */
-(void) startBeaconMonitoring;

/**
 *  Force beacon ranging.
 * This can be use to detect more actively beacons but is a little bit more energy eating.
 */
-(void) startRanging;

/**
 *  Method to call to trigger notification appearance
 *
 *  @return BOOL Whether or not we received a KNotification
 */
-(BOOL) isKNotificationReceived: (UILocalNotification * __nullable)localNotification;

@end
