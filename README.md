# Documentation techniuqe SDK KairosFire

![Kairosfire](https://bytebucket.org/kairosibeacon/kairosfire-sdk-pod/raw/9a67103bb755ec3e93cf7ab8e2c6e27d3aa6716b/doc/kairosfire-rgb.png)


## Guide d'intégration du SDK sous Xcode / iOS

### Etape 1: Installation du SDK

#### Installation via cocoapods

Nous conseillons fortement une installation via cocoapods, si celui-ci n'est pas déjà installé, lancez cette commande dans le terminal : ```sudo gem install cocoapods```.

Créez ou complétez ensuite le fichier Podfile à la racine de votre projet Xcode :

Pour les versions de Cocoapods < 1.0:

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/kairosfire-sdk-pod.git'

platform :ios, '8.0'
pod 'KairosBeaconManager', '~> 1.3.2'
```

À partir de la version 1.0 de Cocoapods :

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/kairosfire-sdk-pod.git'

platform :ios, '8.0'
target your_target 
	pod 'KairosBeaconManager', '~> 1.3.2'
```

Exécutez ensuite la commande ```pod install```.

> **⚠️ Attention :** 
> À partir de cette étape, il faudra dorénavant utiliser le fichier ```.xcworkspace``` pour ouvrir votre projet Xcode à la place de ```.xcproj```

#### Installation manuelle du SDK

##### Intégration de la librairie 
Vous devez copier les fichiers ```libKairosBeaconManager```, ```KExtrasResources.bundle``` et ```KairosBeaconManager.h``` dans le projet courant. Le plus simple est de glisser-déposer les fichiers dans l'interface de Xcode.

##### Ajout des dépendances

Il est nécessaire d'installer manuellement dans votre projet les dépendances suivantes :

GoogleAnalytics v3.13 : [lien sur le site de Google Analytics](https://developers.google.com/analytics/devguides/collection/ios/v3/sdk-download)

AFNetworking v2.6.3 : [Lien sur github](https://github.com/AFNetworking/AFNetworking/archive/2.6.3.zip)

### Etape 2 : Edition du fichier Plist

L'édition du fichier Plist permet de personnaliser le message de l'écran de demande d'autorisation d'accès aux données de localisation en tâche de fond de l'application.

> ⚠️ Cette modification est **nécessaire** pour le bon fonctionnement du SDK Kairos.

Il est possible de laisser une chaîne de caractères vide (comme dans l'illustration ci-dessous).

Cependant, **nous incitons fortement l'application mobile à utiliser cet espace pour expliquer à l'utilisateur la valeur ajoutée de l'autorisation**.

Dans le fichier PList du projet, rajoutez la clef ```NSLocationAlwaysUsageDescription``` avec en valeur un texte qui invitera l'utilisateur à autoriser l'application à mobile à accéder aux données de localisation en tâche de fond.

Voici un exemple de fenêtre popup qui s'affichera, "Votre Application" sera remplacé par le nom de votre application et le texte intégré dans votre fichier PList sera affiché à la suite de la popup :

![popup](https://bytebucket.org/kairosibeacon/kairosfire-sdk-pod/raw/9a67103bb755ec3e93cf7ab8e2c6e27d3aa6716b/doc/notification3.png)

### Etape 3 : Modification du fichier AppDelegate.m
Pour initialiser le SDK, il est nécessaire d'ajouter les lignes suivantes dans la méthode  ```didFinishLaunchingWithOptions```:
> **⚠️ Attention :** 
> Ne pas oublier de remplacer ```app_secret``` et ```app_token``` par les valeurs fournies.


```objectivec
#import "AppDelegate.h"
#import "KairosBeaconManager.h"
#import "Constants.h"

@interface AppDelegate()<KairosBeaconManagerDelegate>

@property (strong, nonatomic) KairosBeaconManager *manager;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application 
didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
     /** ⚠️ Mandatory : initialize beacon manager **/
    self.manager = [KairosBeaconManager sharedInstance:@"app_secret" 
                                         appToken:@"app_token" 
                                         delegate:self];
    
    // optionnal
    [self.manager setDebug:YES];
    
    // ⚠️ mandatory start monitoring for kairos beacons
    [self.manager startBeaconMonitoring];
    
    return YES;
}

/** ⚠️ Mandatory : enable local notification **/
- (void)application:(UIApplication*)application 
            didReceiveLocalNotification:(UILocalNotification*)notification{
    
    if (![self.manager isKNotificationReceived:notification]){
        //write non-kairos notification
    }
    
}

```


### Etape 4 (optionnelle) : Recherche active de balises iBeacon

Par défaut, le SDK écoute de manière passive les iBeacons à proximité (mode monitoring actif tout le temps, y compris lorsque l'application est en background). 
En utilisant la méthode ```startRanging``` on peut demander au SDK d'effectuer une recherche plus aggressive des iBeacons à proximité, cette méthode ne fonctionnera que lorsque l'application est active.  
Pour activer ce mode, on peut ajouter la ligne suivante dans la méthode ```applicationDidBecomeActive``` :

```objectivec
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self.manager startRanging];
}

```

```startRanging``` va activer le ranging de balises iBeacon pendant la durée ```scanDelay``` (voir Annexe > API).


### Etape 5 (Optionnelle) : Paramétrage de la réception des notifications

Il est possible de paramétrer le SDK pour désactiver ou non l'affichage des notifications push. Nous utilisons pour cela les valeurs stockées dans ```NSUSerDefault```. Cela permet au développeur d'applications d'intégrer facilement dans sa page de paramètres un switch pour activer ou non les notifications.

Par défaut, l'affichage des notifications est actif.
Pour activer / désactiver l'affichage des notifications, vous pouvez utiliser le code suivant :

```objectivec
// Importer KairosBeaconManager.h permet d'avoir accès à la constante "kAllowKNotification" 
// qui contient la valeur de la clef utilisée dans NSUserDefault
#import "KairosBeaconManager.h"

[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAllowKNotification];

```


## Migration du SDK v1.0 vers v1.3

> **⚠️ Attention :** 
> Le SDK v1.3 a été compilé pour les versions d'iOS >= 8.

### Remplacement des fichiers du SDK

Pour effectuer la migration du SDK v1.0 à v1.3, il est nécessaire de supprimer les fichiers ```libibeacon.a```, ```ExtrasResources.bundle``` et ```KairosBeaconManager.h``` puis de les remplacer par les nouveaux ```libibeacon.a```, ```KExtrasResources.bundle``` et ```KairosBeaconManager.h```.

### Modification du constructeur du ```KairosBeaconManager```

Il faut remplacer ```apiKey``` par ```appToken``` dans l'appel d'initialisation du ```KairosBeaconManager```. 

```objectivec
[KBeaconManager sharedInstance:@"app_secret" 
                      appToken:@"app_token" //précédemment apiKey
                      delegate:self]
```


### Modification de la méthode du ```application  didReceiveLocalNotification ```

Dans la méthode ```application: didReceiveLocalNotification:``` il faut  modifier: 

```objectivec
- (void) application:(UIApplication *)application 
        didReceiveLocalNotification:(UILocalNotification *)notification{
    
    if (![self.manager isKairosNotificationReceived]){
        //write non-kairos notification
    }
}
```
 Par :
 
```objectivec
- (void) application:(UIApplication *)application 
        didReceiveLocalNotification:(UILocalNotification *)notification{
    
    if (![self.manager isKNotificationReceived:notification]){
        //write non-kairos notification
    }
}
```   
 

## Annexe

### API :

Les méthodes disponibles sont les suivantes :

1. ```(void)setDebug:(BOOL)YES|NO``` active l'affichage des logs de l'application
2. ```(void)setScanDelay:(NSInteger)delay``` paramètre la durée d'un scan lorsque l'application est en mode ```ranging```. La valeur par défaut est de 8 secondes. Il n'est pas conseillé d'augmenter cette valeur car cela peut avoir un impact négatif sur la durée de la batterie. 
3. ```(void)startBeaconMonitoring``` démarre le monitoring d'iBeacon par le manager
4. ```(void)startRanging``` force le ranging d'iBeacons à proximité
5. ```(BOOL)isKNotificationReceived``` permet de savoir si la notification reçue est une ```KNotification```


## Contacts :

Technique : [Alexandre  Assouad](mailto:assouad@kairosfire.com)

Responsable partenariats : [Pierre-Louis Picot](mailto:picot@kairosfire.com)


